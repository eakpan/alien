#include <SFML/Graphics.hpp>
#include <time.h>
#include <list>
#include <cmath>
#include <fstream>
#include <iostream>
#include <SFML/Audio.hpp>
#include <string>
//ADDITIONS: Gave bullets sound, added scores, killing ufo now gives a one hit shield
using namespace sf;
using namespace std;

const int W = 1200; //Dimensions of the window
const int H = 800;

float DEGTORAD = 0.017453f; //used to convert degrees to radians

class Animation { //animation class; allows textures to be used for entities, holds dimensions/location and animation speed
public:
    float Frame, speed;
    Sprite sprite;
    std::vector<IntRect> frames;

    Animation() {}

    Animation(Texture &t, int x, int y, int w, int h, int count, float Speed) {
        Frame = 0;
        speed = Speed;

        for (int i = 0; i < count; i++) {
            frames.push_back(IntRect(x + i * w, y, w, h));
        }

        sprite.setTexture(t); //puts the texture in the center of the sprite
        sprite.setOrigin(w / 2, h / 2);
        sprite.setTextureRect(frames[0]);
    }


    void update() { //iterates through the frames of an animation
        Frame += speed;
        int n = frames.size();
        if (Frame >= n) { Frame -= n; }
        if (n > 0) { sprite.setTextureRect(frames[int(Frame)]); }
    }

    bool isEnd() { //used to determine if an animation is complete
        return Frame + speed >= frames.size();
    }

};


class Entity {
public:
    float x, y ; //x,y is the location of the entity (asteroid, player, etc),
    float dx,dy; //dx,dy is the speed of entity
    float R, angle; //R is collision radius, angle is the rotation angle of the entity
    bool life; //draw the entity if life is true, remove from display list if false
    std::string name; //describe the derived class being used
    Animation anim; //object that performs the animation of entities (like explosions etc)

    Entity() { //wether an entity should/is on screen
        life = 1;
    }

    void settings(Animation &a, int X, int Y, float Angle = 0, int radius = 1) { //creates an animation object, x and y ints,
        // an angle for the texture to be rendered at and an effective radius for the "hurtbox"
        anim = a;
        x = X;
        y = Y;
        angle = Angle;
        R = radius;
    }

    virtual void update() {};

    void draw(RenderWindow &app) { //default render location/appearance of the sprite
        anim.sprite.setPosition(x, y);
        anim.sprite.setRotation(angle + 90);
        app.draw(anim.sprite);

        CircleShape circle(R); //makes a default circle that serves as the most basic entity
        circle.setFillColor(Color(255, 0, 0, 170));
        circle.setPosition(x, y);
        circle.setOrigin(R, R);
        //app.draw(circle);
    }

    virtual ~Entity() {};
};


class asteroid : public Entity {
public:
    asteroid() {//asteroid has a random speed and loops on the screen edge

        dx = rand() % 8 - 4;
        dy = rand() % 8 - 4;
        name = "asteroid";
        //asteroid count goes up for each time the constructor is called
        ++count;
        //cout<<"asteroid made"<<endl;

    }
    ~asteroid();

    void update() {
        x += dx;
        y += dy;
        //periodically moves asteroid from one end of the screen to the other if it hits the ledge
        if (x > W) { x = 0; }
        if (x < 0) { x = W; }
        if (y > H) { y = 0; }
        if (y < 0) { y = H; }
    }

    unsigned int getCount(){return count;}
    //static int getNextCount(){return nextCount;}

private: //private count member
    static unsigned int count;
   //keeps count of how many asteroids are instantiated

};
bool noRocks = false;

unsigned int asteroid::count{0};
//unsigned int asteroid::getCount() { return count;}
//de-con for the asteroid class, decrements upon asteroid destruction
asteroid::~asteroid(){
    --count;
    //cout<<"asteroid destroyed, remaining: "<< getCount() <<endl;
    if(getCount()==0){
        noRocks = true;
        //cout<<"asteroids gone: "<< noRocks <<endl;
    }
}


class bullet : public Entity { //a entity with a preset trajectory, dies on the edge
public:
    bullet() {
        name = "bullet";
    }

    void update() {
        dx = cos(angle * DEGTORAD) * 6;
        dy = sin(angle * DEGTORAD) * 6;
        // angle+=rand()%6-3;
        x += dx;
        y += dy;
        //set life to false if it hits the ledge
        if (x > W || x < 0 || y > H || y < 0) { life = 0; }
    }

};
bool shield = false;

class player : public Entity { //the player, wraps around on edges, can control the direction of its movement
public:
    bool thrust;

    player() {
        name = "player";
    }
    //allows player to accelerate to a top speed if thrust is true(button pressed). If thrust is false it comes to a stop
    void update() {
        if (thrust) {
            dx += cos(angle * DEGTORAD) * 0.2;
            dy += sin(angle * DEGTORAD) * 0.2;
        } else {
            dx *= 0.99;
            dy *= 0.99;
        }

        int maxSpeed = 15;
        float speed = sqrt(dx * dx + dy * dy);
        if (speed > maxSpeed) {
            dx *= maxSpeed / speed;
            dy *= maxSpeed / speed;
        }

        x += dx;
        y += dy;
        //allows player to wrap around to other side of the screen
        if (x > W) { x = 0; }
        if (x < 0) { x = W; }
        if (y > H) { y = 0; }
        if (y < 0) { y = H; }
    }

};
//ensures there's only one ufo out
bool ufoOut = false;


//the new ufo entity
class ufo : public Entity {
public:
    ufo() { //it only moves from left to right, disappears on the edge
        dx = 3;//rand() % 4;
        dy = 0; //rand() % 8 - 4;
        name = "ufo";
    }

    void update() {
        x += dx;
        y += dy;
        //should make the sprite disappear at the edge of screen
        if (x > W || x < 0 || y > H || y < 0) {
            life = 0;
            ufoOut=false;
        }
    }
};


bool isCollide(Entity *a, Entity *b) { //determines if two entities are touching
    return (b->x - a->x) * (b->x - a->x) +
           (b->y - a->y) * (b->y - a->y) <
           (a->R + b->R) * (a->R + b->R);
}

int score =0;
int main() {
    //creating the window
    srand(time(0));

    RenderWindow app(VideoMode(W, H), "Asteroids!");
    app.setFramerateLimit(60);

    // the buffer and sound variables for the sounds
    sf::SoundBuffer buffer;
    sf::Sound ufoSound;

    if (!buffer.loadFromFile("sounds/zapsplat_rin.ogg")){
        std::cout<<"failed to load sound"<<std::endl;
        return EXIT_FAILURE;
    }else{
        ufoSound.setBuffer(buffer);
    }

    sf::SoundBuffer buffer1;
  //  sf::Sound bulletSound;

    if (!buffer1.loadFromFile("sounds/zapsplat_laser.ogg")){
        std::cout<<"failed to load sound"<<std::endl;
        return EXIT_FAILURE;
    }
    sf::Sound bulletSound;
    bulletSound.setBuffer(buffer1);

    //Open-Sans font from https://www.1001freefonts.com/, Licence: Public Domain
    sf::Font font;
    if (!font.loadFromFile("fonts/OpenSans-Regular.ttf"))
    {
        std::cout<<"failed to load font"<<std::endl;
        return EXIT_FAILURE;
        // error...
    }

    sf::Text scoreTally; //The score text that is displayed
    String scoreText = "Score: ";
    scoreTally.setFont(font);
    scoreTally.setString(scoreText + std::to_string(score));
    scoreTally.setCharacterSize(14);

    scoreTally.setFillColor(sf::Color::Yellow);
    scoreTally.setStyle(sf::Text::Bold | sf::Text::Underlined);

    //sounds: zapsplat_science_fiction_50s_style_synthesized_accent_alients_weird_theremin_002_44799.
    //zapsplat_transport_bicycle_bell_ring_001_16730
    //sci-fi musical accent and bike ring from https://www.zapsplat.com/?s=alien&post_type=music&sound-effect-category-id, Licence: non-commercial, commercial and broadcast production

    Texture t1, t2, t3, t4, t5, t6, t7;
    Texture t8; //ufo sprite
    t1.loadFromFile("images/spaceship.png"); //the textures that are used on entities
    t2.loadFromFile("images/background.jpg");
    t3.loadFromFile("images/explosions/type_C.png");
    t4.loadFromFile("images/rock.png");
    t5.loadFromFile("images/fire_blue.png");
    t6.loadFromFile("images/rock_small.png");
    t7.loadFromFile("images/explosions/type_B.png");

    //ufo.png image from https://www.flaticon.com/search?word=ufo
    //Licence: Free for personal and commercial purpose with attribution.
    t8.loadFromFile("images/ufo.png");


    t1.setSmooth(true);
    t2.setSmooth(true);

    Sprite background(t2);

   // animation objects

    Animation sExplosion(t3, 0, 0, 256, 256, 48, 0.5);
    Animation sRock(t4, 0, 0, 64, 64, 16, 0.2);
    Animation sRock_small(t6, 0, 0, 64, 64, 16, 0.2);
    Animation sBullet(t5, 0, 0, 32, 64, 16, 0.8);
    Animation sPlayer(t1, 40, 0, 40, 40, 1, 0);
    Animation sPlayer_go(t1, 40, 40, 40, 40, 1, 0);
    Animation sExplosion_ship(t7, 0, 0, 192, 192, 64, 0.5);

    Animation sUFO(t8, 0, 0, 64, 64, 2, 0);


    std::list<Entity *> entities;
//The higher i goes to, the more the game starts with.
    for (int i = 0; i < 15; i++) {
        asteroid *a = new asteroid();
        a->settings(sRock, rand() % W, rand() % H, rand() % 360, 25);
        entities.push_back(a);
    }



    /* ufo entity details
        ufo *u = new ufo();
        ufoSound.play();
        u->settings(sUFO, 100, rand() % H, 270, 20);
        entities.push_back(u); */

    player *p = new player();
    p->settings(sPlayer, 200, 200, 0, 20);
    entities.push_back(p);



    /////main loop/////
    while (app.isOpen()) { //this is the loop that registers the collisions
        Event event;
        while (app.pollEvent(event)) {
            if (event.type == Event::Closed) {
                app.close();
            }

            if (event.type == Event::KeyPressed) {

                if (event.key.code == Keyboard::Space) {
                    bulletSound.play();

                    bullet *b = new bullet();
                    b->settings(sBullet, p->x, p->y, p->angle, 10);
                    entities.push_back(b);
                }
            }
        }

        if (Keyboard::isKeyPressed(Keyboard::Right)) { p->angle += 3; }
        if (Keyboard::isKeyPressed(Keyboard::Left)) { p->angle -= 3; }
        if (Keyboard::isKeyPressed(Keyboard::Up)) { p->thrust = true; }
        else { p->thrust = false; }


        for (auto a:entities) {
            for (auto b:entities) { //this allows the bullet to destroy the asteroid
                if (a->name == "asteroid" && b->name == "bullet") {
                    if (isCollide(a, b)) {
                        a->life = false;
                        b->life = false;
                        score+=5;

                        Entity *e = new Entity();
                        e->settings(sExplosion, a->x, a->y);
                        e->name = "explosion";
                        entities.push_back(e);


                        for (int i = 0; i < 2; i++) {
                            if (a->R == 15) continue;
                            Entity *e = new asteroid();
                            e->settings(sRock_small, a->x, a->y, rand() % 360, 15);
                            entities.push_back(e);
                        }

                    }
                }// ufo-bullet collision code
                if (a->name == "ufo" && b->name == "bullet") {
                    if (isCollide(a, b)) {
                        a->life = false;
                        b->life = false;
                        ufoOut = a->life;
                        score+=10;
                        shield = true;

                        Entity *e = new Entity();
                        e->settings(sExplosion, a->x, a->y);
                        e->name = "explosion";
                        entities.push_back(e);
                        ufoSound.stop();


                    }
                }
                // //////
                if (a->name == "player" && b->name == "asteroid") {
                    if (isCollide(a, b)) {
                        b->life = false;

                        Entity *e = new Entity();
                        e->settings(sExplosion_ship, a->x, a->y);
                        e->name = "explosion";
                        entities.push_back(e);


                        if(shield!=true){
                            p->settings(sPlayer, W / 2, H / 2, 0, 20);
                            p->dx = 0;
                            p->dy = 0;
                        }else{
                            score+=5;
                            shield = false;
                        }
                    }
                }
                //ufo-player collision

                if (a->name == "player" && b->name == "ufo") {
                    if (isCollide(a, b)) {
                        b->life = false;
                        ufoOut =false;
                        ufoSound.stop();

                        Entity *e = new Entity();
                        e->settings(sExplosion_ship, a->x, a->y);
                        e->name = "explosion";
                        entities.push_back(e);

                        p->settings(sPlayer, W / 2, H / 2, 0, 20);
                        p->dx = 0;
                        p->dy = 0;
                    }
                }
            }
        }

        //shows the blue jets on the rocket
        if (p->thrust) {
            p->anim = sPlayer_go;
        }else{
                p->anim = sPlayer;
            }

        for (auto e:entities) {
            if (e->name == "explosion") {
                if (e->anim.isEnd()) { e->life = 0; }
            }
        }

        //Here is the code that will make new asteroids
        if (rand() % 150 == 0) {
            asteroid *a = new asteroid();//create picture, x and y starting location, angle and collision radius in entity list
            a->settings(sRock, 0, rand() % H, rand() % 360, 25);
            entities.push_back(a);
        }
        //this statement resets the game when there are no asteroids
        if(noRocks==true){
            //cout<<"*********************"<<endl;
            noRocks=false;
            for (int i = 0; i < 15; i++) {
                asteroid *a = new asteroid();
                a->settings(sRock, rand() % W, rand() % H, rand() % 360, 25);
                entities.push_back(a);
            }
        }

        //here is the random timer for making ufos
        if (rand() % 400 == 0 && ufoOut==false) {
            ufoOut = true;
            ufo *u = new ufo();
            ufoSound.play();
            u->settings(sUFO, 50, rand() % H, 270, 20);
            entities.push_back(u);
        }
        //entities.begin() will return starting iterator of the entities list
        //entities.end() will return the theoretical iterator of the element after the last element on the list
        // i is an iterator to u entity pointer
        for (auto i = entities.begin(); i != entities.end();) {
            Entity *e = *i; //*i is an entity pointer, using * on an iterator returns the element from the list

            e->update();    //use polymorphism to call the proper update method
            e->anim.update(); //move to the next frame of the animation

            //if the entity's life is false then it gets removed from the list
            if (e->life == false) {
                i = entities.erase(i); //move the iterator to the next element in the list
                delete e; //delete the object pointed to by e via polymorphism used to call the derived class deconstructor
            }
            else { i++; } //if life was true, move iterator to the next element in the list
        }


        //////draw//////
        app.draw(background);
        scoreTally.setString(scoreText + std::to_string(score));
        app.draw(scoreTally); //displays the score
        //auto determines what is in the container automatically

        for (auto i:entities) {
            i->draw(app);
        }

        app.display();
    }

    return 0;
}
